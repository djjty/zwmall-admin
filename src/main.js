import Vue from 'vue';
import App from './App.vue';
import './plugins/element.js';
import store from './store';
import router from './router';

import util from './common/tools/util.js';
import regular from './common/tools/regular.js';

import imageUploader from './components/uploader/image-uploader.vue';

// 全局组件
Vue.component('image-uploader', imageUploader);

// 全局变量
Vue.prototype.$util = util;
Vue.prototype.$regular = regular;

// 禁用启动时提示当前是在开发模式下
Vue.config.productionTip = false;

new Vue({
	store,
	router,
	render: h => h(App)
}).$mount('#app');
