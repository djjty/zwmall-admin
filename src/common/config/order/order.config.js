export default {
	statuses: [{
			code: -1,
			name: '全部'
		}, {
			code: 0,
			name: '待付款'
		},
		{
			code: 2,
			name: '已付款'
		},
		{
			code: 4,
			name: '已配货'
		},
		{
			code: 6,
			name: '已发货'
		},
		{
			code: 8,
			name: '已收货'
		},
		{
			code: 10,
			name: '已评价'
		},
		{
			code: 30,
			name: '退款中'
		},
		{
			code: 32,
			name: '已退款'
		},
		{
			code: 34,
			name: '退款失败'
		},
		{
			code: 40,
			name: '已取消'
		}
	],
	pickerOptions: {
		shortcuts: [{
			text: '最近一周',
			onClick(picker) {
				const end = new Date();
				const start = new Date();
				start.setTime(start.getTime() - 3600 * 1000 * 24 * 7);
				picker.$emit('pick', [start, end]);
			}
		}, {
			text: '最近一个月',
			onClick(picker) {
				const end = new Date();
				const start = new Date();
				start.setTime(start.getTime() - 3600 * 1000 * 24 * 30);
				picker.$emit('pick', [start, end]);
			}
		}, {
			text: '最近三个月',
			onClick(picker) {
				const end = new Date();
				const start = new Date();
				start.setTime(start.getTime() - 3600 * 1000 * 24 * 90);
				picker.$emit('pick', [start, end]);
			}
		}]
	},
	expressCompanys: ['顺丰', '京东', '圆通', '韵达', '申通', '中通', 'EMS', '邮政', '百世', '天天', '德邦', '如风达'],
	steps: [{
			key: 'createTime',
			name: '下单'
		},
		{
			key: 'paidTime',
			name: '付款'
		},
		{
			key: 'preparedTime',
			name: '配货'
		},
		{
			key: 'shippedTime',
			name: '发货'
		},
		{
			key: 'receiptedTime',
			name: '收货'
		},
		{
			key: 'cancelTime',
			name: '取消'
		},
		{
			key: 'refundTime',
			name: '退款'
		}
	]
}
