export default {
	orderChartOptions: {
		legend: {
			right: 0,
			itemWidth: 14,
			itemGap: 20,
			icon: 'roundRect',
			selectedMode: false
		},
		tooltip: {
			trigger: 'item',
			formatter: '￥{c}',
			backgroundColor: '#fff',
			textStyle: {
				color: '#ff6700'
			},
			extraCssText: 'box-shadow: 0 0 10px rgba(0, 0, 0, .1);'
		},
		grid: {
			left: 50,
			bottom: 20,
			right: 0,
			top: 70
		},
		xAxis: {
			type: 'category',
			data: [],
			axisLine: {
				lineStyle: {
					color: '#e0e0e0'
				}
			},
			axisTick: {
				show: false
			}
		},
		yAxis: {
			type: 'value',
			axisLine: {
				show: false,
				lineStyle: {
					color: '#e0e0e0'
				}
			},
			splitLine: {
				show: false
			},
			axisTick: {
				show: false
			}
		},
		series: [{
			name: '近一年成交额',
			data: [],
			type: 'line',
			smooth: true,
			lineStyle: {
				width: 3
			},
			areaStyle: {
				color: {
					type: 'linear',
					x2: 0,
					y2: 1,
					colorStops: [{
						offset: 0,
						color: '#FFCBA9'
					}, {
						offset: 1,
						color: '#ffffff'
					}]
				}
			}
		}],
		color: ['#ff6700']
	},
	brandChartOptions: {
		legend: {
			right: 0,
			itemWidth: 10,
			itemGap: 20,
			icon: 'circle',
			selectedMode: false,
			orient: 'vertical'
		},
		series: [{
			type: 'pie',
			radius: [60, 100],
			center: ['50%', '50%'],
			label: {
				show: false
			},
			emphasis: {
				label: {
					show: true,
					formatter: '{b}\n销售额：￥{c}\n占比：{d}%'
				}
			},
			data: []
		}],
		color: ['#5554F7', '#61B7FD', '#FEB214', '#69CD92', '#FF6700', '#BFA0BA']
	},
	dataItems: [{
			iconClass: 'el-icon-tickets',
			bgColor: '#61B7FD',
			name: '订单数量',
			valueKey: 'orderCount',
			sub: {
				name: '今日下单',
				valueKey: 'newOrderCount'
			}
		},
		{
			iconClass: 'el-icon-goods',
			bgColor: '#FEB214',
			name: '商品数量',
			valueKey: 'goodsCount',
			sub: {
				name: '今日上新',
				valueKey: 'newGoodsCount'
			}
		},
		{
			iconClass: 'el-icon-user',
			bgColor: '#69CD92',
			name: '用户数量',
			valueKey: 'userCount',
			sub: {
				name: '今日新增',
				valueKey: 'newUserCount'
			}
		},
		{
			iconClass: 'el-icon-loading',
			bgColor: '#FF6700',
			name: '待付款订单',
			valueKey: 'waitingPaidOrderCount',
			sub: {
				name: '今日取消',
				valueKey: 'cancelOrderCount'
			}
		}
	]
}
