export default [{
		path: '/login',
		component: () => import('@/pages/login/index'),
		meta: {
			title: '登录',
			noLogin: true
		}
	},
	{
		path: '/',
		name: 'workbench',
		component: () => import('@/pages/workbench'),
		meta: {
			title: '首页'
		},
		children: [{
				path: '/index',
				component: () => import('@/pages/statistic/index'),
				meta: {
					title: '实时数据'
				}
			},
			{
				path: '/warning-goods',
				component: () => import('@/pages/statistic/warning-goods'),
				meta: {
					title: '库存预警'
				}
			},
			{
				path: '/druid-stat',
				component: () => import('@/pages/statistic/druid-stat'),
				meta: {
					title: '连接池监控'
				}
			},
			{
				path: '/goods',
				component: () => import('@/pages/goods/index'),
				meta: {
					title: '商品管理'
				}
			},
			{
				path: '/goods/detail',
				component: () => import('@/pages/goods/detail'),
				meta: {
					title: '商品详情'
				}
			},
			{
				path: '/goods/comment',
				component: () => import('@/pages/goods/comment/index'),
				meta: {
					title: '评论管理'
				}
			},
			{
				path: '/goods/category',
				component: () => import('@/pages/goods/category/index'),
				meta: {
					title: '商品类别管理'
				}
			},
			{
				path: '/goods/brand',
				component: () => import('@/pages/goods/brand/index'),
				meta: {
					title: '商品品牌管理'
				}
			},
			{
				path: '/goods/attr',
				component: () => import('@/pages/goods/attr/index'),
				meta: {
					title: '商品属性管理'
				}
			},
			{
				path: '/order',
				component: () => import('@/pages/order/index'),
				meta: {
					title: '订单管理'
				}
			},
			{
				path: '/user',
				component: () => import('@/pages/user/index'),
				meta: {
					title: '用户管理'
				}
			},
			{
				path: '/banner',
				component: () => import('@/pages/banner/index'),
				meta: {
					title: '横幅管理'
				}
			},
			{
				path: '/video',
				component: () => import('@/pages/video/index'),
				meta: {
					title: '视频管理'
				}
			},
			{
				path: '/admin/menu',
				component: () => import('@/pages/admin/menu/index'),
				meta: {
					title: '菜单管理'
				}
			},
			{
				path: '/admin/role',
				component: () => import('@/pages/admin/role/index'),
				meta: {
					title: '角色管理'
				}
			},
			{
				path: '/admin/permission',
				component: () => import('@/pages/admin/permission/index'),
				meta: {
					title: '权限管理'
				}
			},
			{
				path: '/admin/administrator',
				component: () => import('@/pages/admin/administrator/index'),
				meta: {
					title: '管理员管理'
				}
			},
			{
				path: '/403',
				component: () => import('@/pages/error/403'),
				meta: {
					title: '权限不足'
				}
			},
			{
				path: '*',
				component: () => import('@/pages/error/404'),
				meta: {
					title: '页面不存在'
				}
			}
		]
	}
]
