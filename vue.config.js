module.exports = {
	lintOnSave: false,
	css: {
		loaderOptions: {
			sass: {
				prependData: `@import '~@/common/styles/variables';`
			}
		}
	},
	devServer: {
		open: true
	},
	productionSourceMap: false
}
