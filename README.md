# zwmall-admin

#### 掌沃商城管理系统前端 - 基于vue+element-ui

#### 项目介绍
掌沃商城（zwmall）是精仿小米官网开发的前后端分离的PC商城

前台预览地址：[掌沃商城](https://zwmall.chengdongqing.top)

后台预览地址：[掌沃商城管理系统](https://admin.zwmall.chengdongqing.top)

在JFinal官网的分享地址：[掌沃商城](https://jfinal.com/share/2127)

在uni-app插件市场的分享地址：[掌沃商城](https://ext.dcloud.net.cn/plugin?id=1431)

#### 源码获取
后端接口：[zwmall-api](https://gitee.com/chengdongqing/zwmall-api)

前台前端：[zwmall-uni](https://gitee.com/chengdongqing/zwmall-uni)

后台前端：[zwmall-admin](https://gitee.com/chengdongqing/zwmall-admin)

#### 项目配置

- 核心框架：Vue 2.6.11

- UI框架：element-ui 2.4.5

- HBuilder X 版本：HBuilder X 2.7.14

### 功能亮点
- 基于echarts的深度图表定制

- 基于element ui的前端界面构建
- 商品的发布及修改，将复杂的业务简单化，轮播图组创建及配置、商品多规格编辑及笛卡尔积的实现、商品概述可包括静态图片和网页代码、基于simditor的富文本编辑器编辑商品参数，编辑商品时支持回显及再次提交
- 单图片、多图片及视频的上传组件封装及视频上传进度条
- 商品类别、品牌等的排序，包括如商品类别的下级类别的排序
- 商品列表的筛选、多规格查询、库存紧张的商品查询等
- 图片广告支持内链和外链，一个页面完成4种图片的管理
- 视频管理支持本地上传及外部链接，支持视频播放
- 权限支持一键同步、菜单支持排序及选择图标等，角色支持配置菜单及权限，权限配置支持同一控制器下的接口全选或取反及回显，菜单配置支持多选及回显，不同角色的配置支持实时刷新渲染等
- 动态菜单，不同管理员不同菜单，对未登录及没有权限等http状态的统一拦截等
- 还有若干未提及的技术点

#### 项目历程
    本项目是我在疫情期间独立开发的商城项目，源自我在学校的毕业项目。毕业两年多，该项目经过不断优化和不同版本的重构开发，今天终于可以和大家见面了。本项目未经过企业级验证，欢迎大家给我反馈问题及提出建议。
    
    路遥  2020-06-01
    邮箱：1912525497@qq.com
    本项目QQ群：550850198

<img src="https://images.gitee.com/uploads/images/2020/0701/152053_ac7e505c_1499515.jpeg" 
height="800">

### 项目说明
    测试账号：19999999999
    密码：123456789

    管理系统账号同上，修改及删除数据将被限制。

    由于通过npm安装的simditor存在一些问题，请在安装项目依赖后用我提供给大家的放在node_modules_simditor下的修复版覆盖simditor的相关包，并在覆盖后删掉该文件夹。